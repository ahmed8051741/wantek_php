<?php


class Database
{
    protected $pdo;

    public function __construct($hostname, $username_db, $password_db, $db_name)
    {
        try {
            $this->pdo = new PDO("mysql:host=$hostname;dbname=$db_name", $username_db, $password_db);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e) {
            echo "error " . $e->getMessage();
        }
    }

   public function query($sql)
    {
        try {
                $this->pdo->prepare($sql);
                $result= $this->pdo->query($sql);
                return $result;
        }
        catch (PDOException $exception) {
            $this->setErrorMessage($exception->getMessage());
            echo $this->getErrorMessage();
            exit();
        }

    }

    public function execute($sql)
    {
        if ($sql !== null) {
            try {
                $this->pdo->exec($sql);
                return true;
            } catch (Exception $exception) {
                $this->setErrorMessage($exception->getMessage());
                return false;
            }
        }
    }






}
?>