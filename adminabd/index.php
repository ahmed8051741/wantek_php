<?php
session_start();
if(isset($_SESSION['username']))
    header("location:pages/layout/View_order.php");
include_once ("../config/input_validation.php");
include_once ("../config/Database.php");
?>

<?php
// فلديشن
$UsernameErr = $passwordErr= $rememberErr = $loginErr = "";
$Username = $id= $password = $remember="";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $val_object = new validation();
    //@ahmed Farhat && osama abu tawhin
    $Username = $val_object->test_input($_POST['username']);
    $password = $val_object->test_input($_POST['password']);
    if(isset($_POST['remember'])) $remember = $val_object->test_input($_POST['remember']);

    $result=$val_object->input_validation($Username," /^[a-zA-Z0-9]+([_ -]?[a-zA-Z0-9])*$/","Enter username","Username must be A-Z numper");
    $UsernameErr=$result[0];
    $Check['Username']=$result[1];

    $result=$val_object->input_validation($password,"^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})^","Enter password","Only Complex password allowed");
    $passwordErr=$result[0];
    $Check['password']=$result[1];

    foreach($Check as $value)
    {
        if($value==false)
        {
            $flag=false;
            break;
        }
        $flag=true;
    }

    if($flag==true){

// DataBase OOP BY Eng Ahmed Farhat

        $DBObject = new Database("localhost", "root", "", "customer");
        $password= md5($password);
        $sql= "SELECT * FROM admin WHERE username = '".$Username."'AND password ='". $password."'";

        $val = $DBObject->query($sql);
        if($row = $val->fetch()) {

            $_SESSION['username']=$Username;

            header("location:pages/layout/View_order.php");
            exit(0);
        }
        $loginErr="invalid Username or password";
    }


}
?>

<!DOCTYPE html>
<html lang="ar">
<head>
    <!-- ENG-Ahmed Farhat -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> login in admin WanTek </title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="vendors/puse-icons-feather/feather.css">
    <link rel="stylesheet" href="vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- ENG-Ahmed Farhat -->
    <!-- inject:css -->
    <link rel="stylesheet" href="css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="images/favicon.png" />
</head>

<body>
<div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
            <div class="row w-100">
                <div class="col-lg-4 mx-auto">
                    <div class="auto-form-wrapper">
                        <form  action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
                            <div class="form-group">
                                <label class="label">Username</label>
                                <div class="input-group">
                                    <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo $Username; ?>">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-check-circle-outline"></i></span>
                                    </div>
                                </div>
                                <span class="error" style="color: red"> <?php echo $UsernameErr; ?></span>

                            </div>

                            <div class="form-group">
                                <label class="label">Password</label>
                                <div class="input-group">
                                    <input type="password" name="password"  class="form-control" placeholder="*********">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="mdi mdi-check-circle-outline"></i></span>
                                    </div>

                                </div>
                                <span class="error" style="color: red"> <?php echo $passwordErr; ?></span>
                                <span class="error" style="color: red" ><?php echo $loginErr?></span>
                            </div>



                            <div class="form-group">
                                <button class="btn btn-primary submit-btn btn-block">Login</button>
                            </div>
                            <div class="form-group d-flex justify-content-between">
                                <div class="form-check form-check-flat mt-0">
                                    <label class="form-check-label">
                                        <input type="checkbox" class="form-check-input" checked>
                                        Keep me signed in
                                    </label>
                                </div>
                                <a href="#" class="text-small forgot-password text-black">Forgot Password</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="js/off-canvas.js"></script>
<script src="js/hoverable-collapse.js"></script>
<script src="js/misc.js"></script>
<script src="js/settings.js"></script>
<script src="js/todolist.js"></script>
<!-- endinject -->
</body>

</html>
