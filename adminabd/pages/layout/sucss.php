<?php
session_start();
if(!isset($_SESSION['username']))
    header("location:index.php");
include_once ("../../../config/input_validation.php");
include_once ("../../../config/Database.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        .err{
            color: red;
        }
    </style>


    <style type="text/css">
        p {
            font-family: "Times New Roman", Times, serif;}
        .body {
            width: 100%;
            margin-bottom: 15px;
            font-size: 16px;
            line-height: 1.5em;
            font-weight: 400;
            font-family:cursive;
        }
        .active{
            background-color: #5a6268;
            border-radius: 10px;
            mso-border-shadow: yes;
        }

    </style>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Radiant Admin Premium Bootstrap Admin Dashboard Template</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="../../vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../../vendors/puse-icons-feather/feather.css">
    <link rel="stylesheet" href="../../vendors/css/vendor.bundle.base.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="../../vendors/morris.js/morris.css">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="../../css/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="../../images/favicon.png" />
</head>
<body class="rtl">
<div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center" style="background-color:#0c85d0">
            <a class="navbar-brand brand-logo" href="View_order.php" style="display: none"> </a>
            <img src="../../images/logo/logo1.png" width="200px" height="80px">
            <a class="navbar-brand brand-logo-mini" href="View_order.php"></a>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-center">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                <span class="mdi mdi-menu"></span>
            </button>




            <div class="navbar-nav navbar-nav-right">

                <a href="Logout.php"><button class="btn-inverse-danger">تسجيل الخروج</button>  </a>
                <a href="../../../index.php" ><button class="btn-inverse-dark"> معاينة الموقع الخاص بك </button>  </a>


            </div>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                <span class="icon-menu"></span>
            </button>
        </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_settings-panel.html -->
        <div class="theme-setting-wrapper">
            <div id="theme-settings" class="settings-panel">
                <i class="settings-close mdi mdi-close"></i>
                <div class="d-flex align-items-center justify-content-between border-bottom">
                    <p class="settings-heading font-weight-bold border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Template Skins</p>
                </div>
                <div class="sidebar-bg-options selected" id="sidebar-dark-theme"><div class="img-ss rounded-circle bg-dark border mr-3"></div>Dark</div>
                <div class="sidebar-bg-options" id="sidebar-light-theme"><div class="img-ss rounded-circle bg-light border mr-3"></div>Light</div>
                <p class="settings-heading font-weight-bold mt-2">Header Skins</p>
                <div class="color-tiles mx-0 px-4">
                    <div class="tiles primary"></div>
                    <div class="tiles success"></div>
                    <div class="tiles warning"></div>
                    <div class="tiles danger"></div>
                    <div class="tiles pink"></div>
                    <div class="tiles info"></div>
                    <div class="tiles dark"></div>
                    <div class="tiles default"></div>
                </div>
            </div>
        </div>
        <div id="right-sidebar" class="settings-panel">
            <i class="settings-close mdi mdi-close"></i>
            <div class="d-flex align-items-center justify-content-between border-bottom">
                <p class="settings-heading font-weight-bold border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Friends</p>
            </div>
            <ul class="chat-list">
                <li class="list active">
                    <div class="profile"><img src="../../images/faces/face1.jpg" alt="image"><span class="online"></span></div>
                    <div class="info">
                        <p>Thomas Douglas</p>
                        <p>Available</p>
                    </div>
                    <small class="text-muted my-auto">19 min</small>
                </li>
                <li class="list">
                    <div class="profile"><img src="../../images/faces/face2.jpg" alt="image"><span class="offline"></span></div>
                    <div class="info">
                        <div class="wrapper d-flex">
                            <p>Catherine</p>
                        </div>
                        <p>Away</p>
                    </div>
                    <div class="badge badge-success badge-pill my-auto mx-2">4</div>
                    <small class="text-muted my-auto">23 min</small>
                </li>
                <li class="list">
                    <div class="profile"><img src="../../images/faces/face3.jpg" alt="image"><span class="online"></span></div>
                    <div class="info">
                        <p>Daniel Russell</p>
                        <p>Available</p>
                    </div>
                    <small class="text-muted my-auto">14 min</small>
                </li>
                <li class="list">
                    <div class="profile"><img src="../../images/faces/face4.jpg" alt="image"><span class="offline"></span></div>
                    <div class="info">
                        <p>James Richardson</p>
                        <p>Away</p>
                    </div>
                    <small class="text-muted my-auto">2 min</small>
                </li>
                <li class="list">
                    <div class="profile"><img src="../../images/faces/face5.jpg" alt="image"><span class="online"></span></div>
                    <div class="info">
                        <p>Madeline Kennedy</p>
                        <p>Available</p>
                    </div>
                    <small class="text-muted my-auto">5 min</small>
                </li>
                <li class="list">
                    <div class="profile"><img src="../../images/faces/face6.jpg" alt="image"><span class="online"></span></div>
                    <div class="info">
                        <p>Sarah Graves</p>
                        <p>Available</p>
                    </div>
                    <small class="text-muted my-auto">47 min</small>
                </li>
            </ul>
        </div>
        <!-- partial -->
        <!-- partial:../../partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
                <li class="nav-item"> <a class="nav-link" href="View_order.php"> <img class="menu-icon" src="../../images/menu_icons/01.png" alt="menu icon"> <span class="menu-title " style="font-size:large;padding-top: 0.3px;font-family:cursive;"> ادارة طلبات المستخدمين </span></a> </li>
                <li class="nav-item " >
                    <a class="nav-link" data-toggle="collapse" href="#apps-dropdown" aria-expanded="true" aria-controls="apps-dropdown"> <img class="menu-icon" src="../../images/menu_icons/09.png" alt="menu icon"> <span class="menu-title"style="font-size:large;padding-top: 0.3px;font-family:cursive;">ادارة حسابات لوحة التحكم </span><i class="menu-arrow"></i></a>
                    <div class="collapse show" id="apps-dropdown">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item"> <a class="nav-link" href="register.php" style="font-size:16px; font-family:Centaur; padding-right: 20%"> اضافة مستعمل جديد </a> </li>
                            <li class="nav-item"> <a class="nav-link" href="update_user.php" style="font-size:16px; font-family:Centaur; padding-right: 20%"> تعديل بينات حسابك </a> </li>
                        </ul>
                    </div>
            </ul>
        </nav>

        <!-- partial -->



        <div class="main-panel">
            <div class="content-wrapper">
                <!-- محتوى الصفحة-->
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                    <div class="form-group" >
                     </div>

                    <div class="accordion-inverse-success" style="text-align: center;border-radius: 2px">
                        <h2> تمت العملية بنجاح   </h2>
                    </div>


                    <div style="text-align: center">


                    </div>

                </form>

            </div>



        <footer class="footer" style="float: left;text-align: left">
            <div class="container-fluid clearfix">
                <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Ahmed Farhat © 2021 <a href="" target="_blank">Bootstrapdash</a>. All rights reserved.</span>
                <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with <i class="mdi mdi-heart text-danger"></i></span>
            </div>
        </footer>
        <!-- partial -->
    </div>
    <!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script src="../../vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../../vendors/chart.js/Chart.min.js"></script>
<script src="../../vendors/raphael/raphael.min.js"></script>
<script src="../../vendors/morris.js/morris.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="../../js/off-canvas.js"></script>
<script src="../../js/hoverable-collapse.js"></script>
<script src="../../js/misc.js"></script>
<script src="../../js/settings.js"></script>
<script src="../../js/todolist.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="../../js/dashboard.js"></script>
<!-- End custom js for this page-->
</body>

</html>
