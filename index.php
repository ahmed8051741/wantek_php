<?php
include ("config/input_validation.php");
include_once ("config/Database.php");
?>
<html lang="ar">
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>

<?php
$nameErr = $numperErr = $addErr = $ActivErr="";
$name = $numper = $add =$Active= "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $val_object = new validation();
$Active="غير مكتمل";
    $name = $val_object->test_input($_POST['name']);
    $numper = $val_object->test_input($_POST['numper']);
    $add = $val_object->test_input($_POST['add']);

    $result = $val_object->input_validation($name, "/^[\p{L} ]+$/u", "الرجاء ادخال اسمك ", "لايمكنك كتابة اي رموز داخل حقل الاسم");
    $nameErr = $result[0];
    $Check['name'] = $result[1];

    $result = $val_object->input_validation($numper, "/^[0-9]+$/", "الرجاء ادخال رقم الهاتف", "يسمح فقط بلارقام");
    $numperErr = $result[0];
    $Check['numper'] = $result[1];


    $result = $val_object->selection_validation($add, "الرجاء اختيار مكان التوصيل");
    $addErr = $result[0];
    $Check['add'] = $result[1];


    foreach ($Check as $value) {
        if ($value == false) {
            $flag = false;
            break;
        }
        $flag = true;
    }

    if ($flag == true) {

        // Ahmed Farhat Final exam
        $DBObject = new Database("localhost", "root", "", "Customer");
        $DBObject->query("SET NAMES utf8");
        $DBObject->query("SET CHARACTER SET utf8");
        $sql = "INSERT INTO Customer  (name_Customer, num_Customer, pless,Active )
    VALUES ('$name', '$numper', '$add','$Active')";

        if ($DBObject->execute($sql)) {
            header("location:Sucses.php");
            exit(0);
        }

    }

}
?>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="عسل المجرى .. عسل طبيعي ينتجه النحل المتغذي على زهور المجرى النادرة التي تتواجد على قمم الجبال.">
    <meta name="keywords" content="عسل المجرى">
    <meta property="og:image" content="img/8585-min.png">
    <meta property="og:image:width" content="375">
    <meta property="og:image:height" content="274">
    <!-- Page Title -->
    <title>عسل المجرى القوقازي الفاخر</title>
    <!-- Google Fonts css-->
    <link rel="icon" type="image/vnd.microsoft.icon" href="img/8585-min.png">
    <link rel="shortcut icon" type="image/x-icon" href="img/8585-min.png">
    <!-- Bootstrap css -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <!-- Font Awesome icon css-->
    <link rel="stylesheet" href="css/style.css" crossorigin="anonymous">
    <link href="css/flaticon.css" rel="stylesheet" media="screen">
    <!-- Swiper's CSS -->
    <!-- Animated css -->
    <!-- Main custom css -->
    <link href="css/custom.css" rel="stylesheet" media="screen">
    <link href="css/main.css" rel="stylesheet" media="screen">
   
</head>

<body data-new-gr-c-s-check-loaded="14.983.0" cz-shortcut-listen="true">
    <a href="https://wa.me/966593882311" style="position:fixed; bottom:10px; right:10px; z-index:99; width:80px; hieght:80px;"><img src="img/whats.png" width="100%"></a>
    <div class="wrapper">

        <!-- Header Section Starts-->
        <header class="container">
            <nav dir="rtl" style="background-color: #FFFFFF;box-shadow: 0 6px 6px rgba(0,0,0,.16)"
                class="navbar py-3  navbar-scrollspy  navbar-fixed-top  fixed-top">
                <a class="navbar-brand mr-0" style="font-weight: bold;" href="#">عسل المجرى القوقازي الفاخر</a>
                <div>
                    <a class=" btn-order " href="#contactForm2" style="padding: 8px;font-weight: bold;">اطلب الآن</a>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">س
                        <li class="nav-item active">
                            <a class="nav-link" href="#">الرئيسية <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#about">القيمة الغذائية</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#why-choose">فوائد عسل المجرى</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#faq">رأي الخبير</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#contactForm2">اطلب الآن</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <!-- Header Section ends -->

        <!-- Banner Section Starts -->
        <section class="banner" id="home" style="background-image: url('img/header-min.png'); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;">
            <div class="container">
                <div class="row">
                    <!-- Banner Content start -->
                    <div class="col-md-7 col-sm-8">
                        <div class="header-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp; padding-top: 40px">
                            <h2><span style=" font-size: 35px">عسل المجرى القوقازي الفاخر
</span><br>
                                <span style="display: inline-block;margin-top: 10px; font-size: 22px">
                                    👇 تعريف بسيط بالعسل 👇
                                </span>
                            </h2>

                            <p class="text-right" style="font-size: 16px;font-weight: 500;">

                                عسل المجرى الأبيض هو عسل طبيعي مستخلص من أزهار نبات المجرى النادرة.
                            </p>

                            <div class="text-right btn-wrap">
                                <a href="#contactForm2" class="btn-shopnow"
                                    style="font-size: 20px;font-weight: 800;">اطلب
                                    الآن</a>
                            </div>
                        </div>
                    </div>
                    <!-- Banner Content end -->

                    <!-- Banner Image start -->
                    <div class="col-md-5 col-sm-4">
                        <div class="header-image wow fadeInRight"
                            style="visibility: visible; animation-name: fadeInRight;">
                            <!--<img src="img/8585-min.png" style="width: 300px;" alt="">-->
                            <video width="100%" height="400" controls autoplay>
                              <source src="img/honey.mp4" type="video/mp4">
                            </video>
                        </div>
                    </div>
                    <!-- Banner Image end -->
                </div>
            </div>
        </section>
        <!-- Banner Section Ends -->

        <!-- About Product starts -->
        <section class="about-product" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="section-title wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <h2 class="text-right"><span>عسل المجرى </span><br>
                                <span>عسل طبيعي..عضوي.. نقي</span>
                            </h2>

                            <p
                                style="font-size: 15px;font-weight: bold;direction: rtl;text-align: right;color: #4a3f3f">
                                عسل المجرى الأبيض هو عسل طبيعي يتغذى النحل الذي ينتجه على أزهار المجرى التي لا تظهر إلا على سفوح الجبال، ولا تدوم طويلا، لذلك فهو من الأعسال النادرة ذات الفوائد العظيمة لجميع أجهزة الجسم.

                            </p>
                        </div>
                    </div>


                    <div class="col-md-8">
                        <div class="offerTimer">
                            <h3 class="text-center">خصم 50%</h3>
                            <h4 class="text-center" style="color: #1e7e34">السعر بعد الخصم 395 ر.س</h4>
                            <h4 class="text-center" style="color: #1e7e34">الوزن 1 كيلوجرام</h4>
                            <h2 class="text-center" style="color: #bf130e;font-size: 2.3rem">متبقي على العرض</h2>
                            <div id="flip-timer">
                                
                            </div>
                        </div>
                        <div class="about-entry">
                            <div class="contact-form">
                                <form class="ahmed" id="contactForm1" accept-charset="utf-8" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?> ">
                                    <div class="row">
                                        <div class="form-group text-right col-md-12 col-sm-6">
                                            <input type="text" id="contactForm_name"
                                                class="form-control form-control-lg text-right" name="name" placeholder="الاسم"
                                                required value="<?php echo $name?>">
                                            <div class="help-block with-errors" id="contactForm_nError"></div>
                                            <span class="e"> <?php echo $nameErr?> </span>
                                        </div>
                                        <div class="form-group text-right col-md-12 col-sm-6 ">
                                            <input type="text" id="contactForm_phone"
                                                class="text-right form-control-lg form-control" name="numper" placeholder="رقم الهاتف"
                                                required value="<?php echo $numper?>">
                                            <div class="help-block with-errors" id="contactForm_pError"></div>
                                            <span class="e"> <?php echo $numperErr?> </span>

                                        </div>

                                        <div class="form-group col-md-12 col-sm-6 text-right">
                                            <input type="text" id="contactForm_phone"
                                                   class="text-right form-control-lg form-control" name="add" placeholder="العنوان"
                                                   required value="<?php echo $add?>">
                                            <div class="help-block with-errors" id="contactForm_pError"></div>
                                            <span class="e"> <?php echo $addErr?> </span>
                                        </div>
                                        <span class="e"> <?php echo $addErr?> </span>


                                        <div class="col-md-12 col-sm-12 text-center">
                                            <button type="submit" name="submit" class="btn-contact" id="contactForm_btn"
                                                title="Submit Your Message!">
                                                إرسال
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="about-feature" dir="rtl">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="about-feature-single wow fadeInUp" data-wow-delay="0.2s"
                                        style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                                        <div class="icon-box">
                                            <i class="flaticon-wheat"></i>
                                        </div>
                                        <h3>
                                            علاج فعال للكبار
                                            وغذاء لذيذ الصغار
                                        </h3>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="about-feature-single wow fadeInUp" data-wow-delay="0.4s"
                                        style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                        <div class="icon-box">
                                            <i class="flaticon-leaves"></i>
                                        </div>
                                        <h3>غني بمضادات الأكسدة التي تحارب الجذور الحرة</h3>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="about-feature-single wow fadeInUp" data-wow-delay="0.6s"
                                        style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                        <div class="icon-box">
                                            <i class="flaticon-bee"></i>
                                        </div>
                                        <h3>نقي نقي وخالي من المواد الكيميائية والسكر.</h3>
                                    </div>
                                </div>

                                <div class="col-md-3 col-sm-3 col-xs-6">
                                    <div class="about-feature-single wow fadeInUp" data-wow-delay="0.8s"
                                        style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">
                                        <div class="icon-box">
                                            <i class="flaticon-honey"></i>
                                        </div>
                                        <h3>طبيعي وعضوي 100% وغير مبستر </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Product ends -->

        <!-- Services section starts -->
        <section class="services" id="services">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="services-container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title wow fadeInUp"
                                        style="visibility: visible; animation-name: fadeInUp;">
                                        <h2><span>ما الذي يميز عسل المجرى؟</span></h2>
                                        <p>
                                             يتميز العسل الأبيض بلونه الأبيض ونكهته الخفيفة الشمعية، وبقيمته الغذائية العالية لاحتوائه على نسبة عالية من الألياف الغذائية، والفيتامينات، والمعادن، والعناصر الأساسية التي يحتاجها الجسم مثل
                                           بي1 و بي 2 و بي3 وبي 4 والكالسيوم، والنحاس، والزنك، والحديد، وهذا بدوره يقوي المناعة ويعالج الكثير من الامراض.

                                        </p>



                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <!-- Services single start -->
                                <div class="col-md-6 col-sm-6">
                                    <div class="service-single wow fadeInUp" data-wow-delay="0.2s"
                                        style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">
                                        <div class="icon-box">
                                            <i class="flaticon-title"></i>
                                        </div>
                                        <h3>جودة عالية</h3>
                                        <p>
                                            تم جمع العسل بطريقة آمنة ومحافظة للبيئة، وبدون أي إضافات حافظة أو سكر؛ لضمان
                                            أعلى جودة.
                                        </p>
                                    </div>
                                </div>
                                <!-- Services single end -->

                                <!-- Services single start -->
                                <div class="col-md-6 col-sm-6">
                                    <div class="service-single wow fadeInUp" data-wow-delay="0.4s"
                                        style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                                        <div class="icon-box">
                                            <i class="flaticon-plant"></i>
                                        </div>
                                        <h3>مكونات نقية</h3>
                                        <p>
                                            نحصل على العسل من خلية النحل مباشرة بعد أن يقوم النحل بجمع رحيق زهور المجرى
                                            النادرة، ثم الرجوع إلى الخلية ليقوم بعملية من معقدة يتم بعدها الحصول على عسل
                                            نقي
                                            صافي مائل للأبيض.
                                        </p>
                                    </div>
                                </div>
                                <!-- Services single end -->

                                <!-- Services single start -->
                                <div class="col-md-6 col-sm-6">
                                    <div class="service-single wow fadeInUp" data-wow-delay="0.6s"
                                        style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInUp;">
                                        <div class="icon-box">
                                            <i class="flaticon-shapes"></i>
                                        </div>
                                        <h3>صُنع بحب</h3>
                                        <p>نؤمن نحن بأن العسل معجزة ربانية. يصنعه النحل بكل حب، ونقوم نحن بجمعه بكل حب
                                            لنقدم
                                            لكم الأفضل.</p>


                                    </div>

                                </div>
                                <!-- Services single end -->

                                <!-- Services single start -->
                                <div class="col-md-6 col-sm-6">
                                    <div class="service-single wow fadeInUp" data-wow-delay="0.8s"
                                        style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInUp;">
                                        <div class="icon-box">
                                            <i class="flaticon-gymnast-diet"></i>
                                        </div>
                                        <h3>قيمة غذائية عالية</h3>
                                        <p>يحتوي في تركيبته الطبيعية على العديد من العناصر المعدنية والأملاح
                                            والفيتامينات
                                            التي تجعل منه غذاءً مميزاً وعلاجاً فعالاً للعديد من الأمراض.</p>
                                    </div>
                                </div>
                                <!-- Services single end -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Services section ends -->

        <!-- Features Section Starts -->
        <section class="features" id="feature">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <h2><span>فوائد عسل المجرى للجهاز الهضمي والقولون</span></h2>
                        </div>
                    </div>
                </div>

                <!-- Feature single start -->
                <div class="row feature-single">
                    <div class="col-md-3 col-md-offset-1 col-sm-5 pull-right">
                        <!-- Feature image start -->
                        <div class="feature-image wow fadeInRight"
                            style="visibility: visible; animation-name: fadeInRight;">
                            <img src="img/item1-min.png" alt="">
                        </div>
                        <!-- Feature image end -->
                    </div>

                    <div class="col-md-8 col-sm-7 pull-left">
                        <!-- Feature entry start -->
                        <div class="feature-entry">
                            <h3>محاربة القولون العصبي</h3>
                            <h4>علاج طبيعي يقضى على مشكلة القولون العصبي ويخفف من اوجاعه</h4>
                        </div>
                        <!-- Feature entry end -->
                    </div>
                </div>
                <!-- Feature single end -->

                <!-- Feature single start -->
                <div class="row feature-single">
                    <div class="col-md-3 col-sm-5">
                        <!-- Feature image start -->
                        <div class="feature-image wow fadeInLeft"
                            style="visibility: visible; animation-name: fadeInLeft;">
                            <img src="img/item2-min.png" alt="">
                        </div>
                        <!-- Feature image end -->
                    </div>

                    <div class="col-md-8 col-md-offset-1 col-sm-7">
                        <!-- Feature entry start -->
                        <div class="feature-entry">
                            <h3>غني بمضادات الأكسدة</h3>
                            <h4>وذلك بفضل احتوائه على مركب الفلافونويد الهام جداً لصحة الجسم، مما يجعله يقاوم الجذور
                                والشقوق
                                الحُرة التي تعتبر من أقوى المسببات التي تقف بشكل مباشر وراء الإصابة بالخلايا والأورام
                                السرطانية بأنواعها المختلفة</h4>
                        </div>
                        <!-- Feature entry end -->
                    </div>
                </div>

                <!-- Feature single end -->

                <!-- Feature single start -->
                <div class="row feature-single">
                    <div class="col-md-3 col-md-offset-1 col-sm-5 pull-right">
                        <!-- Feature image start -->
                        <div class="feature-image wow fadeInRight"
                            style="visibility: visible; animation-name: fadeInRight;">
                            <img src="img/item3-min.png" alt="">
                        </div>
                        <!-- Feature image end -->
                    </div>

                    <div class="col-md-8 col-sm-7 pull-left">
                        <!-- Feature entry start -->
                        <div class="feature-entry">
                            <h3>يساعد إلى حد كبير على خفض من معدل الكوليسترول الضار في الدم.</h3>
                            <h4>ويرفع من معدل الكوليسترول الجيد. ما يجعل عسل المجرى الأفضل في معالجة الأمراض المتعلقة
                                بالقلب
                                والأوعية الدموية والشرايين والجلطات.</h4>
                        </div>
                        <!-- Feature entry end -->
                    </div>
                </div>
                <!-- Feature single end -->


                <!-- Feature single start -->
                <div class="row feature-single">
                    <div class="col-md-3 col-sm-5">
                        <!-- Feature image start -->
                        <div class="feature-image wow fadeInLeft"
                            style="visibility: visible; animation-name: fadeInLeft;">
                            <img src="img/item4-min.png" alt="">
                        </div>
                        <!-- Feature image end -->
                    </div>

                    <div class="col-md-8 col-md-offset-1 col-sm-7">
                        <!-- Feature entry start -->
                        <div class="feature-entry">
                            <h3> مضاد حيوي طبيعي</h3>
                            <h4>لأنه يحتوي على مادة الانترفيرون التي تعمل على قتل الفيروسات، وتلك المادة لا توجد سوى في
                                العسل فقط، أما وجود مادة انهيبين في العسل تعمل على التخلص من الفطريات والميكروبات
                                الضارة،
                                وتقتل أي نمو للجراثيم.</h4>
                        </div>
                        <!-- Feature entry end -->
                    </div>
                </div>
                <!-- Feature single end -->
                <div class="row mt-3 bg-light py-3 px-2">
                    <div class="col-md-12">
                        <div class="section-title wow fadeInUp"
                            style="visibility: visible; animation-name: fadeInUp;margin-bottom: 10px;">
                            <h2><span>محاربة القولون العصبي بالعسل الأبيض</span></h2>
                        </div>

                        <p class="text-right" style="font-size: 18px" dir="rtl">
                            تشير الدراسات العلمية إلى إن انتشار متلازمة القولون العصبي تتراوح بين 5% - 20% من مجموع
                            السكان البالغين في العالم العربي وتشكل النساء من بينهم حوالي 75 %، ويعاني بعض مرضى متلازمة
                            القولون العصبي، أيضا، من الاكتئاب أو القلق لكن الرأي السائد بين الخبراء،حاليا، يقول بأن هذه
                            الظواهر ليست هي المسبب لمرض القولون العصبي وان اسبابه كثيرة والسيطرة عليه صعبة، بالأونه
                            الاخيرة اثبت الدراسات بأن العسل الأبيض علاج فعّال للقولون العصبي وأنه أفضل العلاجات الطبيعية
                            للقضاء على مشاكل القولون.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <!-- Features Section ends -->

        <!-- why choose us section starts -->
        <section class="why-choose parallaxie" id="why-choose"
            style="background-image: url(img/why-choose-bg-min.jpg); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; ">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="why-choose-container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title wow fadeInUp"
                                        style="visibility: visible; animation-name: fadeInUp;">
                                        <h2><span>فوائد إضافية لعسل المجرى</span></h2>
                                    </div>
                                </div>
                            </div>


                            <ul class="text-right custom-list">
                                <li>يعوض عسل المجرى الجسم من السكريات المفقودة بسبب المجهود البدني والذهني الذي يقوم به اللاعب أثناء ممارسة التمارين الرياضية.</li>
                                <li>ينظم عسل المجرى ضغط الدم للاعب الذي يحرص على تناوله قبل أداء التمارين.</li>
                                <li> يقوي عسل المجرى عضلة القلب لدى اللاعبين، وذلك لاحتوائه على الجلوكوز الذي يرفع من أداء عضلة القلب لدى اللاعب.</li>
                                <li>يساعد على تنظيم معدل السكر في الدم</li>
                                <li> يعالج مشاكل الجهاز التنفسي</li>
                                <li>يساعد على شفاء الحروق والجروح</li>
                                <li> يقي من الكساح ومفيد للمفاصل.</li>
                                <li>قي من تسوس الأسنان.</li>
                                <li> علاج مفيد للبشرة فهو يعمل على تنقية البشرة من الشوائب ومعالجة البثور وحب الشباب</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- why choose us section ends -->


        <!-- FAQ Section starts -->
        <section class="faqs" id="faq">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <h2><span>ماذا يقول عنه الخبراء؟</span></h2>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="faq-container">
                            <p>
                                بعد التصريح به من وزارة الصحة السعودية والهيئة العامة للغذاء والدواء، تم اعتماد عسل
                                المجرى
                                في المملكة السعودية. وكما وتم إثبات فعاليته الكبيرة وكونه عسل طبيعي وعضوي ونقي 100%
                                وخالي من
                                المواد الحافظة والكيميائية، كما أنه خالي من السكر.

                            </p>
                            <p>
                                وكخبير تغذية،أنصح الجميع ببدء يومهم بملعقة واحدة من عسل المجرى على الريق أول شيء في
                                الصباح.
                                فهو يعمل على إمداد جسمهم بالطاقة والنشاط وزيادة مناعة الجسم، وتحسين صحة الجهاز الهضمي
                                والتخلص من الغازات والانتفاخ.

                            </p>

                            <p>
                                أحمد- طبيب تغذية وأخصائي تخسيس- خبرة 6 سنوات
                            </p>


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- FAQ Section ends -->




        <section class="howToOrder">
            <div class="container">
                <div class="row" dir="rtl">
                    <div class="col-12">
                        <div class="section-title wow fadeInUp text-center"
                            style="visibility: visible; animation-name: fadeInUp;">
                            <h2><span>آلية الطلب</span></h2>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- Contact info single start -->
                        <div class="contact-info-single">


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="contact-single">
                                        <img src="img/1-min.png" alt="">
                                        <h3>قم بتعبئة نموذج الطلب</h3>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="contact-single">
                                        <img src="img/2-min.png" alt="">
                                        <h3>سيتواصل معك المندوب</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Contact info single end -->
                    </div>

                    <div class="col-md-6">
                        <!-- Contact info single start -->
                        <div class="contact-info-single">


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="contact-single">
                                        <img src="img/3-min.png" alt="">
                                        <h3>الدفع عند الاستلام</h3>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="contact-single">
                                        <img src="img/4-min.png" alt="">
                                        <h3>الشحن مجانًا</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Contact info single end -->
                    </div>
                </div>

            </div>
        </section>
        <!-- Contact us section starts -->
        <section class="contact" id="contact">
            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title wow fadeInUp text-center"
                            style="visibility: visible; animation-name: fadeInUp;">
                            <h2><span>احصل على عسل المجرى</span></h2>
                            <p style="font-size: 16px">وتمتع بالصحة والنشاط على الدوام</p>

                            <h4 style="color: #c82333">
                                السعر الأصلي:
                                <del>790 ر.س</del>
                            </h4>
                            <h5 class="my-3" style="font-size: 2rem">
                                خصم 50%
                            </h5>
                            <h3 style="color: #223C55;font-weight: 800;font-size: 1.3rem">
                                السعر بعد الخصم 395 ر.س
                            </h3>
                            <h3 style="color: #223C55;font-weight: 800;font-size: 1.3rem">الوزن 1 كيلوجرام</h3>

                            <h2 style="color: #f7a401">متبقي على العرض</h2>

                            <div id="flip-timer2">
                            
                            </div>
                            <img src="img/a123.png" alt="">

                            <h6>متبقي
                                <span style="background-color: #ea150b;color: #FFFFFF;padding: 2px 5px;">20</span>
                                عبوة فقط
                            </h6>
                        </div>


                        <!-- Contact Form start -->
                        <div class="contact-form">
                            <form class="ahmed" id="contactForm2" accept-charset="utf-8" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?> ">
                                <div class="row">
                                    <div class="form-group col-md-12 text-right col-sm-6">
                                        <input type="text"   name="name" id="contactForm2_name"
                                            class="form-control form-control-lg text-right" placeholder="الاسم"
                                            required value="<?php echo $name ?>">
                                        <div class="help-block with-errors" id="contactForm2_nError"></div>
                                        <span class="e" ><?php echo $nameErr ?></span>
                                    </div>


                                    <div class="form-group col-md-12 text-right col-sm-6">
                                        <input type="text"  name="numper" id="contactForm2_phone"
                                            class="form-control form-control-lg text-right" placeholder="رقم الهاتف"
                                               required value="<?php echo $numper ?>">
                                        <div class="help-block with-errors" id="contactForm2_pError"></div>
                                        <span class="e" ><?php echo $numperErr ?></span>
                                    </div>


                                    <div class="form-group col-md-12 col-sm-6 text-right">
                                        <input type="text"   name="add" id="contactForm2_phone"
                                               class="form-control form-control-lg text-right" placeholder="العنوان"
                                               required value="<?php echo $add ?>">
                                        <div class="help-block with-errors" id="contactForm2_pError"></div>
                                        <span class="e" ><?php echo $addErr ?></span>

                                    </div>


                                    <div class="col-md-12 col-sm-12 text-center">
                                        <button type="submit" name="submit2" class="btn-contact" id="contactForm2_btn" title="Submit Your Message!">
                                            إرسال
                                        </button>
                                    </div>

                                </div>
                            </form>

                            <h6 class="text-center mt-2" style="color: #143064">يشاهده
                                الآن
                                <span id="customersCount" style="color: white;background-color: red;border-radius: 3px;border-width: 6px ">&nbsp;25&nbsp;</span>
                                زائر
                            </h6>
                        </div>
                        <!-- Contact Form end -->
                    </div>
                </div>

            </div>
        </section>
        <!-- Contact us section ends -->

        <!-- Footer section starts -->
        <footer>
            <div class="container">
                <div class="row">
                    <!-- Footer Copyright start -->
                    <div class="col-md-12 col-sm-4">
                        <div class="footer-site-info">
                            <p>جميع الحقوق محفوظة © 2020</p>
                        </div>
                    </div>
                    <!-- Footer Copyright end -->

                    <!-- Footer Menu start -->

                    <!-- Footer Menu end -->

                    <!-- Footer Social start -->

                    <!-- Footer Social end -->
                </div>
            </div>
        </footer>


        <!-- Footer section ends -->


    </div>
    <!-- Jquery Library File -->
    <script src="../../../../../code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous">
    </script>
    <!-- Bootstrap js file -->
    <script src="js/bootstrap.min.js"></script>
    <script>
    var count = 20;
    var num = document.getElementById('#customersCount');
    setInterval(function() {
        // increment "count" by 5 each time setInterval is run
        count = Math.floor(Math.random() * (30 - 20 + 1)) + 20;

        document.getElementById("customersCount").textContent = count;

    }, 2000);
    </script>
    <script>
        $("#contactForm_phone").keypress(function(event){
            var ew = event.which;
            if(ew == 32)
                return true;
            if(48 <= ew && ew <= 57)
                return true;
            if(65 <= ew && ew <= 90)
                return true;
            if(97 <= ew && ew <= 122)
                return true;
            return false;
        });
        $("#contactForm2_phone").keypress(function(event){
            var ew = event.which;
            if(ew == 32)
                return true;
            if(48 <= ew && ew <= 57)
                return true;
            if(65 <= ew && ew <= 90)
                return true;
            if(97 <= ew && ew <= 122)
                return true;
            return false;
        });
    </script>

    <script src="js/timer.js"></script>


</body>


<!-- Mirrored from shshshonline.com/product/honey/sa/page1/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 02 Feb 2021 06:45:08 GMT -->
</html>