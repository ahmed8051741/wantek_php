<html lang="ar">

<!-- Mirrored from shshshonline.com/product/honey/sa/page1/success.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 02 Feb 2021 07:39:06 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta -->

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="عسل المجرى .. عسل طبيعي ينتجه النحل المتغذي على زهور المجرى النادرة التي تتواجد على قمم الجبال.">
    <meta name="keywords" content="عسل المجرى">
    <meta property="og:image" content="img/8585-min.png">
    <meta property="og:image:width" content="375">
    <meta property="og:image:height" content="274">
    <!-- Page Title -->
    <title>عسل المجرى</title>
    <!-- Google Fonts css-->
    <link rel="icon" type="image/vnd.microsoft.icon" href="img/8585-min.png">
    <link rel="shortcut icon" type="image/x-icon" href="img/8585-min.png">
    <!-- Bootstrap css -->
    <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    <!-- Font Awesome icon css-->
    <link rel="stylesheet" href="css/style.css" crossorigin="anonymous">
    <link href="css/flaticon.css" rel="stylesheet" media="screen">
    <!-- Swiper's CSS -->
    <!-- Animated css -->
    <!-- Main custom css -->
    <link href="css/custom.css" rel="stylesheet" media="screen">
    <link href="css/main.css" rel="stylesheet" media="screen">
   
</head>

<body data-new-gr-c-s-check-loaded="14.983.0" cz-shortcut-listen="true">
    <div class="wrapper">

        <!-- Header Section Starts-->
        <header class="container">
            <nav dir="rtl" style="background-color: #FFFFFF;box-shadow: 0 6px 6px rgba(0,0,0,.16)"
                class="navbar py-3  navbar-scrollspy  navbar-fixed-top  fixed-top">
                <a class="navbar-brand mr-0" style="font-weight: bold;" href="#">عسل المجرى</a>
                <div>
                    <a class=" btn-order " href="#contactForm2" style="padding: 8px;font-weight: bold;">اطلب الآن</a>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                   
                </div>
            </nav>
        </header>
        <!-- Header Section ends -->

        <!-- Banner Section Starts -->
        <section class="banner" id="home" style="background-image: url('img/header-min.png'); background-size: cover; background-repeat: no-repeat; background-attachment: fixed; background-position: center;">
            <div class="container">
                <div class="row">
                    <!-- Banner Content start -->
                    <div class="col-md-12 col-sm-12">
                        <div class="header-content wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            <h2><span>شكرا جزيلا على ثقتكم</span><br>
                            </h2>

                            <p class="text-right" style="font-size: 16px;font-weight: 500;">
سيتواصل معك موظف خدمة العملاء عبر الواتساب في أقرب وقت لتأكيد عنوانك                                <br>

                            </p>

                            <div class="text-right btn-wrap">
                                <a href="https://wa.me/966593882311" class="btn-shopnow"
                                    style="font-size: 20px;font-weight: 800;">
                                    تواصل عبر الواتساب</a>
                            </div>
                        </div>
                    </div>
                    <!-- Banner Content end -->
                </div>
            </div>
        </section>
        <!-- Banner Section Ends -->
        
    </div>
    <!-- Jquery Library File -->
    <script src="../../../../../code.jquery.com/jquery-3.3.1.slim.min.js" crossorigin="anonymous">
    </script>
    <!-- Bootstrap js file -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/timer.js"></script>
</body>


<!-- Mirrored from shshshonline.com/product/honey/sa/page1/success.php by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 02 Feb 2021 07:39:09 GMT -->
</html>